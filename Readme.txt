Team members:
Shreya Rebecca Nagelli A04227721
Meghana Kolturi A04300290
Are Sri Harsha  A04272136

Description:
1. Term search:
        First we loop through all the available mailboxes in the dataset of enron.
        for each mailbox we have opened the mailbox and loop through all the mails in the mailbox.
        for each mailbox we tokenized the body and checked if the token list has all the search terms.
        if the mail has all the search terms print it to output.


2. Address Search:
        First we loop through all the available mailboxes in the dataset of enron and select the mailboxes related to the person.
        For each mailbox we have opened the mailbox and loop through all the mails in the mailbox.
        For each mailbox we check the To and From fields of the mail for first and last names.
        If the terms has first and last names print it to output.

3. Interaction Search:
        First we loop through all the available mailboxes in the dataset of enron and select the mailboxes related to the 2 persons.
        For each mailbox we have opened the mailbox and loop through all the mails in the mailbox.
        For each mailbox we check the To and From fields of the mail against the 2 mails given in input.
        If matched, we print the output.

Pre - requisites 

Java latest
Python latest

Computation process:

1. Open code folder.

2.  DownloadtheEnronemaildatasetfrom https://www.cs.cmu.edu/~enron/ and place in code folder.
    Be sure to get the May 7, 2015 version. The file will be named
    enron_mail_20150507.tar.gz.

3.  In terminal enter -  tar xvfz enron_mail_20150507.tgz  --- extract data from archive file 

4.  In terminal enter - ./count_messages.sh

5.  ./count_messages.sh | cut -d' ' -f1 | awk '{s+=$1} END {print s}'
        out put -- 517401

6.  Now in terminal run ./convert_enron_to_mbox.py

7.  Now in terminal run -  ls enron | wc

8.  Now in terminal run - javac -cp lib/activation.jar:lib/javax.mail.jar:lib/mbox.jar ReadMbox.java

9.  Now in terminal run - java -cp .:lib/activation.jar:lib/javax.mail.jar:lib/mbox.jar ReadMbox enron/enron.allen-p._sent_mail

10.  Now in terminal run - ./verify_mbox.sh > mbox.log &

11.  Now in terminal run - cut -d' ' -f3 mbox.log | awk '{s+=$1} END {print s}'
        output should be - 517401

 ------------ By here data set is ready --------------



compilation of java program
// javac -cp lib/activation.jar:lib/javax.mail.jar:lib/mbox.jar enron_search.java

first command example
// java -cp .:lib/activation.jar:lib/javax.mail.jar:lib/mbox.jar enron_search term_search All evidence hide

second command example
// java -cp .:lib/activation.jar:lib/javax.mail.jar:lib/mbox.jar enron_search address_search  John Arnold

third command example
// java -cp .:lib/activation.jar:lib/javax.mail.jar:lib/mbox.jar enron_search interaction_search  andrew.fairley@enron.com john.arnold@enron.com

