import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.search.SearchTerm;
import javax.activation.*;
import java.util.*;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

public class enron_search {

    // public static ConsoleHelper consoleHelper = new ConsoleHelper();

    public static void main(String[] args) throws Exception {

        if (args.length < 2) {
            System.out.println("Please enter search action and query to search");
            return;
        }
        String action = args[0];
        switch (action) {
            case "term_search":
                term_search(args);
                break;
            case "address_search":
                address_search(args);
                break;
            case "interaction_search":
                interaction_search(args);
                break;
            default:
                System.out.println(
                        "Please check your preferred action. The possible actions are\n 1. term_search\n 2. address_search\n 3. interaction_search\n");
                return;
        }
    }

    public static void term_search(String[] args) throws Exception {

        Set<String> termList = new HashSet<String>();

        for (int x = 1; x < args.length; x++) {
            termList.add(args[x].toLowerCase());
            // System.out.println(args[x]);
        }

        Session session = Session.getDefaultInstance(new Properties());
        Store store = session.getStore(new URLName("mbox:"));

        store.connect();
        List<String> resultList = new ArrayList<>();

        List<String> names = getNames();

        for (int k = 0; k < names.size(); k++) {
            // for (String foldername : getNames()) {
            Folder folder = store.getFolder("enron/" + names.get(k));
            folder.open(Folder.READ_ONLY);

            // creates a search criterion
            SearchTerm searchCondition = new SearchTerm() {
                @Override
                public boolean match(Message message) {
                    try {

                        StringTokenizer stringTokenizer = new StringTokenizer(
                                message.getContent().toString().toLowerCase(),
                                " .,");
                        List<String> tokens = new ArrayList<>();

                        while (stringTokenizer.hasMoreTokens())
                            tokens.add(stringTokenizer.nextToken());
                        if ((tokens).containsAll(termList))
                            return true;
                    } catch (MessagingException | IOException | NullPointerException ex) {
                        ex.printStackTrace();
                    }
                    return false;
                }
            };

            // performs search through the folder
            Message[] messages = folder.search(searchCondition);

            for (int x = 0; x < messages.length; x++) {
                try {
                    resultList.add(messages[x].getFrom()[0].toString() + " " + messages[x].getSentDate());
                } catch (AddressException ad) {
                    String date = "";
                    String name = "";
                    for (Enumeration<Header> e = messages[x].getAllHeaders(); e.hasMoreElements();) {
                        Header h = e.nextElement();
                        if (h.getName().equals("From"))
                            name = h.getValue();
                        if (h.getName().equals("Date"))
                            date = h.getValue();
                    }
                    resultList.add(name + " " + date);
                    // System.out.println();
                    // System.out.println(name+" "+date);
                }
            }
            folder.close();
            System.out.print(" \rProcessing  " + (k + 1) + "/" + names.size() + " folders");
        }
        store.close();
        System.out.println();
        for (int x = 0; x < resultList.size(); x++)
            System.out.println(x + 1 + ". " + resultList.get(x));
        System.out.println("\n\nResults Found :  " + resultList.size());

    }

    public static void address_search(String[] args) throws Exception {
        if (args.length != 3) {
            System.out.println("Please enter last_name and first name");
            return;
        }
        String first_name = args[1];
        String last_name = args[2];
    

        Session session = Session.getDefaultInstance(new Properties());
        Store store = session.getStore(new URLName("mbox:"));

        store.connect();
        Set<String> resultList = new HashSet<String>();

        List<String> names = getFolderNames(last_name, first_name);

        for (int k = 0; k < names.size(); k++) {
            // for (String foldername : getNames()) {
            Folder folder = store.getFolder("enron/" + names.get(k));
            folder.open(Folder.READ_ONLY);
            // creates a search criterion
            SearchTerm searchCondition = new SearchTerm() {
                @Override
                public boolean match(Message message) {
                    try {
                        for (Enumeration<Header> e = message.getAllHeaders(); e.hasMoreElements();) {
                            Header h = e.nextElement();
                            if (h.getValue().equals(first_name + " " + last_name)
                            || h.getValue().equals(last_name + " " + first_name) 
                            || h.getValue().equals(first_name + ", " + last_name)
                            || h.getValue().equals(last_name + ", " + first_name) )
                                return true;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return false;
                }
            };

            // performs search through the folder
            Message[] messages = folder.search(searchCondition);

            for (int x = 0; x < messages.length; x++) {
                try {
                    for (Enumeration<Header> e = messages[x].getAllHeaders(); e.hasMoreElements();) {
                        Header h = e.nextElement();
                        if (h.getValue().equals(first_name + " " + last_name)
                        || h.getValue().equals(last_name + " " + first_name) 
                        || h.getValue().equals(first_name + ", " + last_name)
                        || h.getValue().equals(last_name + ", " + first_name) ) {
                            if (h.getName().equalsIgnoreCase("X-From"))
                                resultList.add(messages[x].getFrom()[0].toString());
                            if (h.getName().equalsIgnoreCase("X-To"))
                                resultList.add(messages[x].getHeader("To")[0].toString());
                        }
                    }

                } catch (MessagingException | NullPointerException ex) {
                    ex.printStackTrace();
                }
            }
            folder.close();
            System.out.print(" \rProcessing  " + (k + 1) + "/" + names.size() + " folders");
            // animate(k+"/"+names.size());
        }
        store.close();
        System.out.println("\n");
        String[] finals = resultList.toArray(String[]::new);
        for (int x = 0; x < resultList.size(); x++)
            System.out.println(x + 1 + ". " + finals[x]);
        System.out.println("\nResults Found :  " + resultList.size());

    }

    public static void interaction_search(String[] args) throws Exception {
        if (args.length != 3) {
            System.out.println("Please enter addresses of two people");
            return;
        }
        String person1 = args[2];
        String person2 = args[1];
        Session session = Session.getDefaultInstance(new Properties());
        Store store = session.getStore(new URLName("mbox:"));

        store.connect();

        Set<String> resultList = new HashSet<String>();

        List<String> names = getFolderNamesByMail(person1, person2);

        for (int k = 0; k < names.size(); k++) {
            // for (String foldername : getNames()) {
            Folder folder = store.getFolder("enron/" + names.get(k));
            folder.open(Folder.READ_ONLY);
            // creates a search criterion
            SearchTerm searchCondition = new SearchTerm() {
                @Override
                public boolean match(Message message) {
                    try {
                        if ((message.getFrom()[0].toString().equalsIgnoreCase(person1)
                                && message.getHeader("To")[0].toString().equalsIgnoreCase(person2))
                                || (message.getFrom()[0].toString().equalsIgnoreCase(person2)
                                        && message.getHeader("To")[0].toString().equalsIgnoreCase(person1)))
                            return true;
                    } catch (Exception e) {
                       
                    }
                    return false;
                }
            };

            // performs search through the folder
            Message[] messages = folder.search(searchCondition);

            for (int x = 0; x < messages.length; x++) {
                try {
                    if ((messages[x].getFrom()[0].toString().equalsIgnoreCase(person1)
                            && messages[x].getHeader("To")[0].toString().equalsIgnoreCase(person2)))
                        resultList.add(person1 + " -> " + person2 + "  [Subject : " + messages[x].getSubject() + "] "
                                + messages[x].getSentDate());
                    else
                        resultList.add(person2 + " -> " + person1 + "  [Subject : " + messages[x].getSubject() + "] "
                                + messages[x].getSentDate());
                                // resultList.add(person2 + " -> " + person1 + "  [Subject : " + messages[x].getSubject() + "] "
                                // + messages[x].getSentDate()+"-----"+ names.get(k));
                } catch (MessagingException | NullPointerException ex) {
                    ex.printStackTrace();
                }
            }
            folder.close();
            System.out.print(" \rProcessing  " + (k + 1) + "/" + names.size() + " folders");
            // animate(k+"/"+names.size());
        }
        store.close();
        System.out.println("\n");
        String[] finals = resultList.toArray(String[]::new);
        for (int x = 0; x < resultList.size(); x++)
            System.out.println(x + 1 + ". " + finals[x]);
        System.out.println("\nResults Found :  " + resultList.size());
    }

    public static List<String> getNames() {
        FileFilter filter = new FileFilter() {
            public boolean accept(File f) {
                return !f.isDirectory();
            }
        };
        File[] directories = new File("enron").listFiles(filter);

        List<String> names = new ArrayList<>();
        for (int x = 0; x < directories.length; x++) {
            names.add(directories[x].getName());
        }
        return names;
    }

    public static List<String> getFolderNames(String first, String last) {
        FileFilter filter = new FileFilter() {
            public boolean accept(File f) {
                return !f.isDirectory();
            }
        };
        File[] directories = new File("enron").listFiles(filter);

        List<String> names = new ArrayList<>();
        for (int x = 0; x < directories.length; x++) {
            if (directories[x].getName().toLowerCase().contains(first.toLowerCase())
                    || directories[x].getName().toLowerCase().contains(last.toLowerCase()))
                names.add(directories[x].getName());
        }
        return names;
    }

    public static List<String> getFolderNamesByMail(String mail1, String mail2) {
        FileFilter filter = new FileFilter() {
            public boolean accept(File f) {
                return !f.isDirectory();
            }
        };
        File[] directories = new File("enron").listFiles(filter);

        List<String> tokens = new ArrayList<>();

       
        String[] arr=  mail1.split("[@._]");
        for (String name : arr)
            tokens.add(name.toLowerCase());
                 
        String[] arr2=  mail2.split("[@._]");
        for (String name : arr2)
            tokens.add(name.toLowerCase());

            List<String> remove = new ArrayList<>();
            remove.add("enron");
            remove.add("com");
            tokens.removeAll(remove);

        Set<String> names = new HashSet<>();
        for (int x = 0; x < directories.length; x++) {
            for (String token : tokens)
                if (directories[x].getName().contains(token)) {
                    names.add(directories[x].getName());
                    // System.out.println(directories[x].getName());
                }
        }

        return new ArrayList<>(names);
    }

}


//  compilation
// javac -cp lib/activation.jar:lib/javax.mail.jar:lib/mbox.jar enron_search.java

// first command
// java -cp .:lib/activation.jar:lib/javax.mail.jar:lib/mbox.jar enron_search term_search Saturday

// second command
// java -cp .:lib/activation.jar:lib/javax.mail.jar:lib/mbox.jar enron_search address_search  John Arnold

//third command
// java -cp .:lib/activation.jar:lib/javax.mail.jar:lib/mbox.jar enron_search interaction_search  andrew.fairley@enron.com john.arnold@enron.com