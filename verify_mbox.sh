#!/bin/bash

FILES=enron/*

for f in $FILES
do
  echo -n "Processing $f: "
  java -cp .:lib/activation.jar:lib/javax.mail.jar:lib/mbox.jar ReadMbox $f | grep "Number of messages" | cut -d':' -f 2 | sed -e 's/^[[:space:]]*//'
done